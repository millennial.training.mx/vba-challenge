Attribute VB_Name = "Module1"
Sub tickerCrawler()
    Dim x, y, rowCount, tickerCount, sheetsCount, currentSheet As Integer
    Dim ticker As String
    Dim valStart, valEnd, yearChange As Double
    Dim totalVolume, percentChange As LongLong
    
    For Each ws In Worksheets
        ws.Activate
        'get las row in current worksheet
        lastRow = ws.UsedRange.Rows.Count
        'set headers for report
        Cells(1, 9).Value = "Ticker"
        Cells(1, 10).Value = "YearlyChange"
        Cells(1, 11).Value = "Percent Change"
        Cells(1, 12).Value = "Total Stock Volume"
        'initialize values for the active ws
        tickerCount = 2 'skips header in report section
        ticker = ""
        totalVolume = 0
        valStart = 0
        valEnd = 0
        yearChange = 0
        percentChange = 0
        
        
        For x = 2 To lastRow
            'Get current ticker name
            ticker = Cells(x, 1).Value
            'Get val at beginning of year for this ticker
            If valStart = 0 Then
                valStart = Cells(x, 3).Value
            End If
            'sum the totalVolume values for this ticker
            totalVolume = totalVolume + ws.Cells(x, 7)
            'check if next ticker will be different than current.
            If Cells(x + 1, 1).Value <> ticker Then
                'we are on last row for this ticker, report completed totals
                valEnd = Cells(x, 6).Value
                yearChange = valEnd - valStart
                
                ' Calculate percent change value
                If valStart = 0 Then
                    percentChange = 0
                Else
                    percentChange = (yearChange / valStart) * 100
                End If
                
                Cells(tickerCount, 9).Value = ticker
                Cells(tickerCount, 10).Value = yearChange
                Cells(tickerCount, 11).NumberFormat = "0%"
                Cells(tickerCount, 11).Value = (percentChange / 100)
                Cells(tickerCount, 12).Value = totalVolume
                
                'Conditional formatting of colors for yearChange.
                If yearChange > 0 Then
                    Cells(tickerCount, 10).Interior.ColorIndex = 4
                ElseIf yearChange < 0 Then
                    Cells(tickerCount, 10).Interior.ColorIndex = 3
                Else
                    Cells(tickerCount, 10).Interior.ColorIndex = 6
                End If
                
                'reset values back to 0 for next ticker
                tickerCount = tickerCount + 1
                totalVolume = 0
                valStart = 0
                valEnd = 0
                yearChange = 0
                percentChange = 0
            End If
        Next x
        
        'section to display top tickers per sheet
        Range("O2").Value = "Greatest % Increase"
        Range("O3").Value = "Greatest % Decrease"
        Range("O4").Value = "Greatest Total Volume"
        Range("P1").Value = "Ticker"
        Range("Q1").Value = "Value"
        
        ' Initialize variables and values for greatest percent increase
        greatest_percent_increase_ticker = Cells(2, 9).Value
        greatest_percent_increase = Cells(2, 11).Value
        ' Initialize variables and values for greatest percent decrease
        greatest_percent_decrease_ticker = Cells(2, 9).Value
        greatest_percent_decrease = Cells(2, 11).Value
        ' Initialize variables and values for greatest volume
        greatest_stock_volume_ticker = Cells(2, 9).Value
        greatest_stock_volume = Cells(2, 12).Value
        
        'get last row for ticker reports section in this ws
        lastRowReport = ws.Cells(Rows.Count, "I").End(xlUp).Row
        
        'loop through the list of ticker results.
        For i = 2 To lastRowReport
            ' Find greatest percent increase.
            If Cells(i, 11).Value > greatest_percent_increase Then
                greatest_percent_increase = Cells(i, 11).Value
                greatest_percent_increase_ticker = Cells(i, 9).Value
            End If
            ' Find greatest percent decrease.
            If Cells(i, 11).Value < greatest_percent_decrease Then
                greatest_percent_decrease = Cells(i, 11).Value
                greatest_percent_decrease_ticker = Cells(i, 9).Value
            End If
            ' Find greatest stock volume.
            If Cells(i, 12).Value > greatest_stock_volume Then
                greatest_stock_volume = Cells(i, 12).Value
                greatest_stock_volume_ticker = Cells(i, 9).Value
            End If
            
        Next i
        
        ' Report values for greatest percent increase, decrease, and stock volume for this ws.
        Range("P2").Value = greatest_percent_increase_ticker
        Range("Q2").Value = greatest_percent_increase
        Range("Q2").NumberFormat = "0%"
        
        Range("P3").Value = greatest_percent_decrease_ticker
        Range("Q3").Value = greatest_percent_decrease
        Range("Q3").NumberFormat = "0%"
        
        Range("P4").Value = greatest_stock_volume_ticker
        Range("Q4").Value = greatest_stock_volume
        
    Next ws
End Sub
